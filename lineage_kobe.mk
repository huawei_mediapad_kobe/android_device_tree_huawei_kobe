 #
# Copyright (C) 2019-2020 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/languages_full.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)
$(call inherit-product, device/huawei/kobe/device.mk)


## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := kobe
PRODUCT_NAME := lineage_kobe
PRODUCT_BRAND := HUAWEI
PRODUCT_MODEL := KOB-W09
PRODUCT_MANUFACTURER := HUAWEI

# TODO: ignore for now
#PRODUCT_GMS_CLIENTID_BASE := android-huawei

# Use the latest approved GMS identifiers unless running a signed build
ifneq ($(SIGN_BUILD),true)
PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DISC="KOB-W09-user 7.0 HUAWEIKOB-W09 C100B278 release-keys" \
    TARGET_DEVICE="KOB-W09"
endif
BUILD_FINGERPRINT := HUAWEI/KOB/HWKobe-Q:7.0/HUAWEIKOB-W09/C100B278:user/release-keys